<?php
/*
 *  Author: Lenlay
 */
define('LWP', 'lwp', true);

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('2160w', 2160, '', true);
    add_image_size('1920w', 1920, '', true);
    add_image_size('1024w', 1040, '', true);
    add_image_size('640w', 600, '', true);
}

function lwp_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}

add_filter('upload_mimes', 'lwp_mime_types');

/*------------------------------------*\
	Functions
\*------------------------------------*/

// HTML5 Blank navigation
function lwp_nav( $location = 'main-menu' )
{
	wp_nav_menu(
	array(
		'theme_location'  => $location,
		'menu'            => '',
		'container'       => 'div',
		'container_class' => "menu-{$location}-container",
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}


// Register Navigation
function register_lwp_menu()
{
    register_nav_menus(array(
        'main-menu' => __('Main Menu', LWP),
    ));
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'teatrhotel'),
        'description' => __('Description for this widget-area...', LWP),
        'id' => 'widget-area-1',
        'before_widget' => '<ul class="off-canvas-list">',
        'after_widget' => '</ul>',
        'before_title' => '<li><label><h3>',
        'after_title' => '</h3></label></li>'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function lwp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages,
        'prev_text'    => '<svg class="next-icon" width="51.9px" height="51.9px" viewBox="0 0 51.9 51.9" xml:space="preserve"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-prev-bold"></use></svg>',
        'next_text'    => '<svg class="next-icon" width="51.9px" height="51.9px" viewBox="0 0 51.9 51.9" xml:space="preserve"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-next-bold"></use></svg>',
    ));
}

// Custom Excerpts
function lwp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function lwp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function lwp_excerpt($length_callback = 'lwp_custom_post', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom Comments Callback
function lwpcomments($comment, $args, $depth)
{
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);
    $add_below = 'comment';
?>
    <li <?php comment_class(empty($args['has_children']) ? '' : 'parent') ?> id="comment-<?php  comment_ID() ?>">
    <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
        <div class="comment-author vcard">
            <?php if ($args['avatar_size'] != 0) echo get_avatar($comment, $args['180']); ?>
            <?php printf(__('<cite class="fn">%s</cite>') , get_comment_author_link()) ?>
        </div>
        <?php if ($comment->comment_approved == '0'): ?>
            <em class="comment-awaiting-moderation"><?php
                _e('Your comment is awaiting moderation.') ?></em>
        <?php endif; ?>

        <div class="comment-meta commentmetadata"><a href="<?php
            echo htmlspecialchars(get_comment_link($comment->comment_ID)) ?>">
            <?php printf(__('%1$s at %2$s') , get_comment_date() , get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)') , '  ', ''); ?>
        </div>
        <div class="comment-text">
        <?php comment_text() ?>
        </div>
        <div class="reply">
            <?php comment_reply_link(array_merge($args, array(
                'add_below' => $add_below,
                'depth' => $depth,
                'max_depth' => $args['max_depth']
            ))); ?>
        </div>
    </div>
    </li>
<?php
}

/*------------------------------------*\
	Actions + Filters
\*------------------------------------*/

// Add Actions
add_action('init', 'register_lwp_menu'); // Add HTML5 Blank Menu

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
// add_filter('acf/settings/show_admin', '__return_false');//  Hide ACF field group menu item

add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

include_once 'inc/loader.php';
