<?php
//enqueue assets
include 'enqueue.php';

include_once 'post_types.php';

// ACF Metaboxes
include_once( get_stylesheet_directory() . '/inc/acf_config.php' );
// ACF Theme Settings
include_once( get_stylesheet_directory() . '/inc/acf_theme_settings.php' );
