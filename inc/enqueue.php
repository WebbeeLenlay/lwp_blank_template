<?php

// Load scripts
add_action('wp_enqueue_scripts', 'lwp_header_scripts'); // Add Custom Scripts to wp_head

function lwp_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

        wp_register_script('themescripts', get_template_directory_uri() . '/assets/js/app.js', array('jquery'), filemtime(get_template_directory() . '/assets/js/app.js')); // Custom scripts
        wp_enqueue_script('themescripts');

    }
}


// Load styles
add_action('wp_enqueue_scripts', 'lwp_styles'); // Add Theme Stylesheet

function lwp_styles()
{
    wp_register_style('themestyle', get_template_directory_uri() . '/assets/css/app.css', array(), filemtime(get_template_directory() . '/assets/css/app.css'), 'all');
    wp_enqueue_style('themestyle');

}
