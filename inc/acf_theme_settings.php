<?php
if( function_exists('acf_add_options_page') ) :

	acf_add_options_page(
		array(
			//'page_title' => 'Theme Settings',
			'menu_title' => 'Theme Settings',
			'menu_slug' => 'theme-settings',
			'capability' => 'edit_posts',
			'redirect' => true,
			'position' => 105,
		)
	);

	acf_add_options_sub_page(
		array(
			'page_title' => 'General Settings',
			'menu_title' => 'General',
			'parent_slug' => 'theme-settings',
		)
	);

	acf_add_local_field_group(
		array (
			'key' => 'group_generaloptions',
			'title' => 'General Options',
			'fields' => array (

			),

		'location' => array (
			array (
				array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-general',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
		));

endif;
?>
