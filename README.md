# LWP Blank Template

- Foundation 6
- ACF configuration ( ACF not included )
- Sass compilation and prefixing
- JavaScript concatenation
- For production builds:
  - CSS compression
  - JavaScript compression
  - Image compression

## Installation

To use this template, your computer needs:
- [Ruby](https://www.ruby-lang.org/en/documentation/installation/)
- [NodeJS](https://nodejs.org/en/) (0.12 or greater)

Then open the folder in your command line, and install the needed dependencies:

```bash
npm install
```

For build and start watching file changes type:

```bash
gulp
```
For production build type:

```bash
gulp build --production
```

##Errors

if you have error 'Error: watch ... ENOSPC':

```bash
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
```
