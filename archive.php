<?php get_header(); ?>

	<?php get_sidebar(); ?>

	<!-- section -->
	<section class="row">
	<div class="small-6 column">
		<?php the_archive_title( '<h1 class="page--title">', '</h1>' ); ?>

		<?php get_template_part('loop'); ?>

		<?php get_template_part('pagination'); ?>

	</div>
	</section>
	<!-- /section -->

<?php get_footer(); ?>
