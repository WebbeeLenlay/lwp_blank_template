function is_touch_device() {
  return 'ontouchstart' in window;
};

(function($){
	var resize_timeout;
	var bgss = new bgsrcset();
	bgss.callonce = false;

	if( is_touch_device() ) {
		$('html').addClass('touch');
	}

	$(window).lazyLoadXT();

	$(window).on('resize', function() {
		if(resize_timeout)
			clearTimeout(resize_timeout);

		resize_timeout = setTimeout(function() {
			$(window).lazyLoadXT();
		}, 300);
	});

	$(document).on('ready', function() {

		bgss.init('.bglazy', function(a){
		  $(a.node).addClass('bg-loaded').trigger('bg-loaded');
		});
  });

})(jQuery);
