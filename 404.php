<?php get_header(); ?>

	<section>

		<!-- article -->
		<article id="post-404">

			<h1 class="page--title"><?php _e( 'Page not found', LWP ); ?></h1>
			<h6>
				<a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', LWP ); ?></a>
			</h6>

		</article>
		<!-- /article -->

	</section>

<?php get_footer(); ?>
