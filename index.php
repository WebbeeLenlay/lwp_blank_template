<?php get_header(); ?>

	<?php get_sidebar(); ?>

	<!-- section -->
	<section>
		<h1 class="page--title"><?php _e( 'Latest Posts', LWP ); ?></h1>

		<?php get_template_part('loop'); ?>

		<?php get_template_part('pagination'); ?>
	</section>
	<!-- /section -->

<?php get_footer(); ?>
